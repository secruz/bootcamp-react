import React from 'react';
import styles from '../styles/styles.module.css'
import alebrije from "../resources/images/alebrije.png";

interface Props{
    readonly titleTechnologies : string;
    readonly titleReact : string;
    readonly titleJpa : string;
    readonly titleSpring : string;
    readonly titleJs : string;
    readonly titleTs : string;

}
export function TechnologiesScreen (props: Props){
    const {titleTechnologies, titleReact, titleJpa, titleJs, titleSpring, titleTs} = props;
    return(
        <div >
        <img
            className={styles.alebrije}
            width="295"
            height="243"
            src={alebrije}
        />

    <div className={styles.page}>

        <h1 className={styles.heading}>{titleTechnologies}</h1>
        <details>
            <summary>{titleReact}</summary>
            <p>React Native is an open-source mobile application framework created by Facebook, Inc. It is used to develop applications for Android, Android TV, iOS, macOS, tvOS, Web, Windows and UWP by enabling developers to use React's framework along with native platform capabilities</p>
        </details>
        <details>
            <summary>{titleJpa}</summary>
            <p>The Java Persistence API (JPA) is a specification of Java. It is used to persist data between Java object and relational database. JPA acts as a bridge betweenobject-oriented domain models and relational database systems.</p>
        </details>
        <details>
            <summary>{titleSpring}</summary>
            <p>Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run". We take an opinionated view of the Spring platform and third-party libraries so you can get started with minimum fuss. Most Spring Boot applications need minimal Spring configuration.</p>
        </details>
        <details>
            <summary>{titleJs}</summary>
            <p>A .js file is a plain text file that contains Javascript scripts, and can therefore be modified with any text editor. It is usually executed by a web browser.</p>
        </details>
        <details>
            <summary>{titleTs}</summary>
            <p>TypeScript is an open-source language which builds on JavaScript, one of the world's most used tools, by adding static type definitions. Types provide a way to describe the shape of an object, providing better documentation, and allowing TypeScript to validate that your code is working correctly</p>
        </details>

    </div>
        </div>
    );

}
