import React from 'react';
import './App.css';
import {Navigator} from "./screen/Navigator";
import {PhilosophyScreen} from "./screen/philosophy-screen";
import {HeaderComponent} from "./components/header-component";
import {WorkManagementcreen} from "./screen/work-management-screen";
import {TechnologiesScreen} from "./screen/technologies-screen";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

export function App() {
  return (
      <Router>
          <HeaderComponent bootcampName="Development Bootcamp Alebrije"/>
          <Navigator/>
          <Switch>
              <Route path="/philosophy" exact component={PhilosophyScreen}><PhilosophyScreen titlePhilosophy={'Philosophy'} titleMission={'Mission'} titleObjective={'Objective'} titlePlan={'Plan'}/></Route>
              <Route path="/workManagement" component={WorkManagementcreen}><WorkManagementcreen titleWorkManagement={'Work Management'}/></Route>
              <Route path="/technologies" component={TechnologiesScreen}><TechnologiesScreen titleTechnologies={'Technologies'} titleReact={'React Native'} titleJpa={'JPA'} titleSpring={'Spring boot'} titleJs={'JavaScript'} titleTs={'Typescript'}/></Route>
          </Switch>
      </Router>



    );
}

export default App;
